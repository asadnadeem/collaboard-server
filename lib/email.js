'use strict';

var nodemailer = require('nodemailer')
  , markdown = require('nodemailer-markdown').markdown;

var GMAIL_USER = "asadnadeem.1993@gmail.com";
var GMAIL_PASS = "dontmesswithme";

var EMAIL_FROM = "Collaboard";
var URL_BASE = "http://localhost:8000/";

var transporter;

var obj = {
  init: function() {
    transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: GMAIL_USER,
        pass: GMAIL_PASS
      }
    });

    transporter.use('compile', markdown());
  },

  invite: function(recipient, room) {
    var link = URL_BASE + "?room=" + room;
    
    transporter.sendMail({
      from: EMAIL_FROM,
      to: recipient,
      subject: "Collaboard Invitation",
      // text: "Come to papa! <a href='" + link + "'>" + link + "</a>"
      markdown: '# Collaboard Invitation\n\n[Join the Collaboard room](' + link + ')\n\n–\nCollaboard Team'
    })
  }
};

module.exports = obj;