'use strict';

var uuid = require('uuid');

var EVENTS = {
  TYPING: 'typing',
  MESSAGE: 'message',
  ALL_MESSAGES: 'all_messages'
};

var NAMESPACE = "chat";

function ChatSession(room) {
  this.room = room;

  this.state = {
    messages: [],
    typing: []
  };
}

ChatSession.prototype.handler = function(socket, data) {

  if (!data.event) return;

  switch(data.event) {
    case EVENTS.TYPING:
      break;

    case EVENTS.MESSAGE:
      this.handleNewMessage(socket, data);
  }

};

ChatSession.prototype.handleNewMessage = function(socket, data) {
  var message = {
    _id: uuid.v1(),
    username: socket.conn.sessionInfo.username,
    text: data.text
  };

  this.state.messages.push(message);

  socket.to(this.room).emit(EVENTS.MESSAGE, message);
};

ChatSession.prototype.emitFullState = function(socket) {
  socket.emit(EVENTS.ALL_MESSAGES, this.state.messages);
};

module.exports = ChatSession;
