var io = require('socket.io')
  , ChatSession = require('./chat')
  , BoardSession = require('./board')
  , emailService = require('./email')
  , faker = require('faker')
  , fs = require('fs');

emailService.init();

var rooms = {};

function onSocketConnection(socket) {
  socket.conn.sessionInfo = {
    username: faker.name.findName()
  };

  process.nextTick(function() {
    ensureSessions();
  });

  socket.emit('nameChanged', socket.conn.sessionInfo.username);
  socket.emit('roomChanged', socket.conn.id);

  socket.on('joinRoom', function(roomId) {
    socket.leave(socket.rooms[0], function() {
      socket.join(roomId, onRoomJoined);
    });
  });

  socket.on('changeName', function(name) {
    socket.conn.sessionInfo.username = name;

    socket.emit('nameChanged', socket.conn.sessionInfo.username);
  });

  socket.on('chat', function(data) {
    if (rooms[socket.rooms[0]].chatSession) {
      rooms[socket.rooms[0]].chatSession.handler(socket, data);
    }
  });

  socket.on('board', function(data) {
    if (rooms[socket.rooms[0]].boardSession) {
      rooms[socket.rooms[0]].boardSession.handler(socket, data);
    }
  });

  socket.on('invite', function(recipient) {
    emailService.invite(recipient, socket.rooms[0]);
  });

  var ensureSessions = function() {
    if (rooms[socket.rooms[0]]) return;

    rooms[socket.rooms[0]] = {
      chatSession: new ChatSession(socket.rooms[0]),
      boardSession: new BoardSession(socket.rooms[0])
    };
  };

  var onRoomJoined = function(err) {
    if (err) {
      console.log("Failed to join room. ", session);
      return;
    }

    ensureSessions();

    if (rooms[socket.rooms[0]].chatSession) {
      rooms[socket.rooms[0]].chatSession.emitFullState(socket);
    }

    if (rooms[socket.rooms[0]].boardSession) {
      rooms[socket.rooms[0]].boardSession.emitFullState(socket);
    }

    socket.emit('roomChanged', socket.rooms[0]);
  };

};

module.exports = {
  run: function() {
    io = io(3000);
    io.on('connection', onSocketConnection);

    console.log("server should be running on 3000");
  }
}