'use strict';

var uuid = require('uuid');

var EVENTS = {
  OBJECT: 'object',
  OBJECT_UPDATE: 'object_update',
  OBJECT_ACK: 'object_ack',
  ALL_OBJECTS: 'all_objects',
  DELETE_OBJECT: 'delete_object'
};

var NAMESPACE = "board";

function BoardSession(room) {
  this.room = room;

  this.state = {
    objects: {}
  };
}

BoardSession.prototype.handler = function(socket, data) {

  if (!data.event) return;

  switch(data.event) {
    case EVENTS.OBJECT:
      this.handleNewObject(socket, data);

    case EVENTS.OBJECT_UPDATE:
      this.handleUpdateObject(socket, data);

    case EVENTS.DELETE_OBJECT:
      this.handleDeleteObject(socket, data);
  }

};

BoardSession.prototype.handleNewObject = function(socket, data) {
  var object = {
    _id: uuid.v1(),
    sockId: socket.conn.id,
    username: socket.conn.sessionInfo.username,
    z: Object.keys(this.state.objects).length
  };

  for (var prop in data.object) {
    object[prop] = data.object[prop];
  }

  this.state.objects[object._id] = object;

  socket.to(this.room).emit(EVENTS.OBJECT, object);
  socket.emit(EVENTS.OBJECT_ACK, object);
};

BoardSession.prototype.handleUpdateObject = function(socket, data) {
  var object = data.object;

  this.state.objects[object._id] = object;

  socket.to(this.room).emit(EVENTS.OBJECT_UPDATE, object);
};

BoardSession.prototype.handleDeleteObject = function(socket, data) {
  delete this.state.objects[data.objectId];

  socket.to(this.room).emit(EVENTS.DELETE_OBJECT, data.objectId);
}

BoardSession.prototype.emitFullState = function(socket) {
  socket.emit(EVENTS.ALL_OBJECTS, this.state.objects);
};

module.exports = BoardSession;